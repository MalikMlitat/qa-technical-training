let cards = []
let sum = 0

let hasBlackJack = false
let isAlive = false
let isGameStarted = false // to be able to run the game only once
let message = ""
let messageEl = document.getElementById("message-el")
let sumEl = document.getElementById("sum-el")
let cardsEl = document.getElementById("cards-el")

let player =
{
    name: "malik",
    chips: 42
}

let playerEl = document.getElementById("player-el")
playerEl.textContent  = player.name + ": $" + player.chips
function getRandomCard()
{
    let randomCard =Math.floor(Math.random() * 13) + 1
    if (randomCard === 1)
    {
        randomCard = 11
    } else if (randomCard in [11, 12, 13])
    {
        randomCard = 10
    }
    return randomCard
}

function startGame()
{
    if (!isGameStarted)
    {
        isGameStarted = true
        cards = [getRandomCard(), getRandomCard() ]
        sum = cards[0] + cards[1]
        isAlive = true
        renderGame()
    }
}

function renderGame()
{
    if (sum < 21)
    {
        message = "draw a new card? :)"
    }
    else if (sum === 21)
    {
        message = "BlackJack :))"
        hasBlackJack = true
        isAlive = false
    }
    else // if (sum > 21)
    {
        message = "OUT OF THE GAME!! :("
        hasBlackJack = false
        isAlive = false
    }

    messageEl.textContent = message
    cardsEl.textContent = "Cards: "
    for (let i = 0; i< cards.length; i++)
    {
        cardsEl.textContent += cards[i] + " "
    }
    sumEl.textContent = "Sum: " + sum
}


function newCard()
{
    if (hasBlackJack === false && isAlive === true)
    {
        let card = getRandomCard()
        sum += card
        cards.push(card)
        renderGame()
    }
}
