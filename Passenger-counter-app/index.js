let countEl = document.getElementById("count-elements")
let savedEl = document.getElementById("saved-elements")
let count = 0


function increment()
{
    count++
    countEl.innerText = count
    console.log(count)
}

function save()
{
    savedEl.textContent += count + ' - '
    console.log("save: " + count)
    // reset the counter
    count = 0
    countEl.innerText = count
}